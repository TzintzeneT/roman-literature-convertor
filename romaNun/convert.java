package romaNun;
public class convert{
	private static int romeCharToNum(char chr){
		if(chr=='I')
			return 1;
		if(chr=='V')
			return 5;
		if(chr=='X')
			return 10;
		if(chr=='L')
			return 50;
		if(chr=='C')
			return 100;
		if(chr=='D')
			return 500;
		if(chr=='M')
			return 1000;
		return 0;
	}
	private static String num1ToRome(int num){
		String str="";
		int i=0;
		String [] arr={"I", "V", "X", "L", "C", "D", "M"};
		for(;num>9;i+=2,num/=10){}
		for(;num>0;num--){
			str+=arr[i];
		}
		if(arr[i]=="M"){
			return str;
		}
		if(str.length()==9){
			str=arr[i]+arr[i+2];
		}
		else if(str.length()>=5){
			for (int j=str.length();j%5!=str.length();){
				str=str.replaceFirst(arr[i], "");
			}
			str=arr[i+1]+str;
		}
		else if(str.length()>3){
			str=arr[i]+arr[i+1];
		}
		return str;
	}
	public static int romeToNum(String str){
		str=str.toUpperCase();
		int num=0;
		for(int i=1, j=str.length()-1;j>-1;j--){
			if(convert.romeCharToNum(str.charAt(j))>=i){
				i=convert.romeCharToNum(str.charAt(j));
				num+=i;
			}
			else{
				num-=convert.romeCharToNum(str.charAt(j));
			}
		}
		return num;
	}
	public static String numToRome(int num){
		String str="";
		for(int i=10;num>0;num-=num%i, i*=10){
			str=convert.num1ToRome(num%i)+str;
		}
		return str;
	}
}